/*
 * Copyright 2013 Thomas Hoffmann
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.j4velin.pedometer;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.j4velin.pedometer.util.API23Wrapper;
import de.j4velin.pedometer.util.Logger;
import de.j4velin.pedometer.util.Util;

import static de.j4velin.pedometer.Pedometer.DAILY_ERR_STEPS;
import static de.j4velin.pedometer.Pedometer.ERR_STEPS;

/**
 * Created on 4/9/2021 by Andrew Slavetskas
 */


public class StepDetectorListener extends Service implements SensorEventListener {

    private static final int stepCadenceMax = 5000;
    private static final int stepCadenceMin = 300;

    private final static long MICROSECONDS_IN_ONE_MINUTE = 60000000;

    private final BroadcastReceiver shutdownReceiver = new ShutdownRecevier();

    private SharedPreferences preferences;
    private static final String LAST_STEP_TIME = "last_step_time";

    @Override
    public void onAccuracyChanged(final Sensor sensor, int accuracy) {
        // nobody knows what happens here: step value might magically decrease
        // when this method is called...
        if (BuildConfig.DEBUG) Logger.log(sensor.getName() + " accuracy changed: " + accuracy);
    }

    @Override
    public void onSensorChanged(final SensorEvent event) {
        if (event.values[0] > Integer.MAX_VALUE) {
            if (BuildConfig.DEBUG) Logger.log("probably not a real value: " + event.values[0]);
            return;
        } else {
            // Get last step detected sensor event timestamp and compare to previous. If cadence is too high or too low then increment erroneous step count
            long lastStepTime = preferences.getLong(LAST_STEP_TIME, 0);
            long eventTime = (System.currentTimeMillis() - SystemClock.elapsedRealtime()) * 1000000 + event.timestamp; // event time in nanoseconds
            long stepTime = eventTime / 1000000;
            int stepDuration = (int) (stepTime - lastStepTime);
            Logger.log("Step duration is " + stepDuration + " ms");

            int runningErroneousSteps = preferences.getInt(ERR_STEPS, 0);
            int dailyErroneousSteps = preferences.getInt(DAILY_ERR_STEPS, 0);
            if(stepDuration > stepCadenceMax){
                // step is over normal cadence, delete step
                runningErroneousSteps++;
                dailyErroneousSteps++;
                Logger.log("Error step, high");
            }else if (stepDuration < stepCadenceMin){
                // step is under normal cadence, delete step
                runningErroneousSteps++;
                dailyErroneousSteps++;
                Logger.log("Error step, low");
            }
            Logger.log("Running error step count is " + runningErroneousSteps);
            Logger.log("Daily error step count is " + dailyErroneousSteps);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(ERR_STEPS, runningErroneousSteps);
            editor.putInt(DAILY_ERR_STEPS, dailyErroneousSteps);
            editor.putLong(LAST_STEP_TIME, stepTime);
            editor.apply();


            /**
             * This section is only for debugging. Not for final release
             */
            /*
            Logger.log("Step detected");
            // go thru step checking algorithm
            int detectedSteps = preferences.getInt("DETECTED_STEPS", 0);
            Logger.log("Steps from storage are " + detectedSteps);
            detectedSteps++;


            // time step was taken
            //long eventTime = (System.currentTimeMillis() - SystemClock.elapsedRealtime()) * 1000000 + event.timestamp; // event time in nanoseconds
            //long stepTime = eventTime / 1000000;
            Logger.log("Step time: " + stepTime); // convert nanoseconds to milliseconds, its close enough for this
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date(stepTime);
            Logger.log("Step date: "  + date);

            // store to preferences
            Logger.log("Storing " + detectedSteps + " into storage");
            //SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("DETECTED_STEPS", detectedSteps);
            editor.commit();

            String[] text = {String.valueOf(detectedSteps), String.valueOf(eventTime / 1000000), String.valueOf(preferences.getInt("STEP_COUNT", 0))};
            // File exist
            try {
                if (f.exists() && !f.isDirectory()) {
                    FileWriter mFileWriter = new FileWriter(filePath, true);
                    writer = new CSVWriter(mFileWriter);
                } else {
                    writer = new CSVWriter(new FileWriter(filePath));
                }
                //String[] data = {"Ship Name", "Scientist Name", "...", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").formatter.format(date)});
                Logger.log("file path: " + filePath);
                writer.writeNext(text);

                writer.close();
            }catch(IOException e){
                e.printStackTrace();
            }

            /**
             * Debugging section above
             */

        }
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        reRegisterSensor();
        registerBroadcastReceiver();

        startForeground(1337, new Notification());

        // restart service every hour to save the current step count
        // this probably isn't needed but I haven't done enough research into it yet (5/1/2021 AS)
        long nextUpdate = Math.min(Util.getTomorrow(), System.currentTimeMillis() + AlarmManager.INTERVAL_HOUR);
        if (BuildConfig.DEBUG) Logger.log("next update: " + new Date(nextUpdate).toLocaleString());
        AlarmManager am = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        PendingIntent pi = PendingIntent.getService(getApplicationContext(), 3, new Intent(this, StepDetectorListener.class), PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= 23) {
            API23Wrapper.setAlarmWhileIdle(am, AlarmManager.RTC, nextUpdate, pi);
        } else {
            am.set(AlarmManager.RTC, nextUpdate, pi);
        }

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) Logger.log("SensorListener onCreate");
    }

    @Override
    public void onTaskRemoved(final Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        if (BuildConfig.DEBUG) Logger.log("sensor service task removed");
        // Restart service in 500 ms
        ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).set(AlarmManager.RTC, System.currentTimeMillis() + 500, PendingIntent
                        .getService(this, 3, new Intent(this, StepDetectorListener.class), 0));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (BuildConfig.DEBUG) Logger.log("SensorListener onDestroy");
        try {
            SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
            sm.unregisterListener(this);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) Logger.log(e);
            e.printStackTrace();
        }
    }


    private void registerBroadcastReceiver() {
        if (BuildConfig.DEBUG) Logger.log("register broadcastreceiver");
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SHUTDOWN);
        registerReceiver(shutdownReceiver, filter);
    }

    private void reRegisterSensor() {
        if (BuildConfig.DEBUG) Logger.log("re-register sensor listener");
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        try {
            sm.unregisterListener(this);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) Logger.log(e);
            e.printStackTrace();
        }

        if (BuildConfig.DEBUG) {
            Logger.log("step detector: " + sm.getSensorList(Sensor.TYPE_STEP_DETECTOR).size());
            if (sm.getSensorList(Sensor.TYPE_STEP_COUNTER).size() < 1) return; // emulator
            Logger.log("detect: " + sm.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR).getName());
        }

        // enable batching with delay of max 5 min
        sm.registerListener(this, sm.getDefaultSensor(Sensor.TYPE_STEP_COUNTER),
                SensorManager.SENSOR_DELAY_NORMAL, (int) (5 * MICROSECONDS_IN_ONE_MINUTE));

        // initiate shared preferences
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }
}
